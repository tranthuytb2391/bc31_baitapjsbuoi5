function TinhTienDien() {
  var hoTen = "";
  var soKw = 0;

  hoTen = document.getElementById("txt-nhap-ho-ten").value;
  soKw = document.getElementById("txt-nhap-so-kw").value * 1;

  var tongTien = 0;

  if (soKw <= 50) {
    tongTien = soKw * 500;
  } else if (soKw <= 100) {
    tongTien = (soKw - 50) * 650 + 50 * 500;
  } else if (soKw <= 200) {
    tongTien = (soKw - 100) * 850 + 50 * 650 + 50 * 500;
  } else if (soKw <= 350) {
    tongTien = (soKw - 200) * 1100 + 100 * 850 + 50 * 650 + 500 * 50;
  } else {
    tongTien = (soKw - 350) * 1300 + 150 * 1100 + 100 * 850 + 50 * 650 + 50 * 500;
  }

  document.getElementById("txtKetQua").textContent = "Họ tên: " + hoTen + "; Tiền điện: " + tongTien.toLocaleString();
}