function TinhDiemDaiHoc(){
    var tongDiem = 0;

    var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
    var diemKhuVuc = document.getElementById("chonKhuVuc").value * 1;
    var diemDoiTuong = document.getElementById("chonDoiTuong").value * 1;

    var diemMon1 = document.getElementById("txt-diem-mon-thu-1").value * 1;
    var diemMon2 = document.getElementById("txt-diem-mon-thu-2").value * 1;
    var diemMon3 = document.getElementById("txt-diem-mon-thu-3").value * 1;

    var thongbaoKeQua = document.getElementById("txtKeQua").textContent;
    tongDiem = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;

    if(diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0){
        thongbaoKeQua = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
    }
    else{
        if(tongDiem >= diemChuan){
            thongbaoKeQua = "Bạn đã đậu. Tổng điểm: " + tongDiem;
        } else {
            thongbaoKeQua = "Bạn đã rớt. Tổng điểm: " + tongDiem;
        }
    }
    document.getElementById("txtKeQua").textContent = thongbaoKeQua;
}
